package com.example.dpdc.moneymanager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Created by DPDC on 10/19/2017.
 */

public class ClothingActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_clothing);

        bindViews();
    }

    private void bindViews() {
    }

    @Override
    public void onClick(View view) {

    }
}
