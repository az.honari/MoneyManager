package com.example.dpdc.moneymanager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

/**
 * Created by DPDC on 10/15/2017.
 */

public class FoodActivity extends BaseActivity implements View.OnClickListener {
    EditText txtName;
    EditText txtPrice;
    Button btnSave;
    Button btnShow;
    TextView txtShow;
    ImageView foodImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        bindViews();

        Hawk.init(mContext).build();

        Picasso.with(mContext).load("https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Foods_%28cropped%29.jpg/48px-Foods_%28cropped%29.jpg").into(foodImage);
    }

    private void bindViews() {
        txtName = (EditText) findViewById(R.id.txtName);
        txtPrice = (EditText) findViewById(R.id.txtPrice);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        btnShow = (Button) findViewById(R.id.btnShow);
        btnShow.setOnClickListener(this);
        txtShow = (TextView) findViewById(R.id.lblShowFood);
        foodImage = (ImageView)findViewById(R.id.foodImage) ;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSave) {

            //Publics.setShared(mContext, "name", txtName.getText().toString());
            //Publics.setShared(mContext, "price", txtPrice.getText().toString());

            Hawk.put("name", txtName.getText().toString());
            Hawk.put("price", txtPrice.getText().toString());
        } else if (view.getId() == R.id.btnShow) {
            String result =
                    "Food Name : " +
                            //Publics.getShared(mContext, "name", "no name")
                            Hawk.get("name", "no name")
                            + "\n" +
                            "Food Price : " +
                            //Publics.getShared(mContext, "price", "no price");
                            Hawk.get("price", "no price");
            txtShow.setText(result);
        }
    }
}
