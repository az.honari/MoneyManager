package com.example.dpdc.moneymanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.widget.TextView;

/**
 * Created by DPDC on 10/15/2017.
 */

public class HomeActivity extends BaseActivity {
    TextView lblWelcome ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bindViews();

        String result;
        result="Hi, " + getIntent().getStringExtra("name") + "\n" + "This is Home Activity!";
        lblWelcome.setText(result);
    }

    private void bindViews() {
        lblWelcome = (TextView ) findViewById(R.id.lblWelcome);
    }

   // @Override
   // public void onBackPressed() {
        //super.onBackPressed();
        //showToast("Hey you, ha ha ha... :D");
    //}
}
