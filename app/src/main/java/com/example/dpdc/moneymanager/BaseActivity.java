package com.example.dpdc.moneymanager;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by DPDC on 10/15/2017.
 */

public class BaseActivity extends AppCompatActivity {
    Context mContext=this;
    Activity mActivity=this;

    public void showToast(String toastText)
    {
        Toast.makeText(mContext, toastText, Toast.LENGTH_SHORT).show();
    }
}
