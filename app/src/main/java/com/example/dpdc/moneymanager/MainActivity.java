package com.example.dpdc.moneymanager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends BaseActivity implements View.OnClickListener {

    Button btnHome;
    Button btnFood;
    EditText txtCall;
    Button btnCall;
    Button btnCar;
    Button btnClothing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();


    }

    private void bindViews() {
        btnHome = (Button) findViewById(R.id.btnHome);
        btnHome.setOnClickListener(this);

        btnFood = (Button) findViewById(R.id.btnFood);
        btnFood.setOnClickListener(this);

        txtCall = (EditText) findViewById(R.id.txtCall);
        btnCall = (Button) findViewById(R.id.btnCall);

        btnCar = (Button) findViewById(R.id.btnCar);
        btnCar.setOnClickListener(this);

        btnClothing = (Button) findViewById(R.id.btnClothing);
        btnClothing.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnHome) {
            Intent homeActivityIntent = new Intent(mContext, HomeActivity.class);
            homeActivityIntent.putExtra("name", "Azin");
            startActivity(homeActivityIntent);
        } else if (view.getId() == R.id.btnFood) {
            Intent foodActivityIntent = new Intent(mContext, FoodActivity.class);
            foodActivityIntent.putExtra("name", "Azin");
            startActivity(foodActivityIntent);
        } else if (view.getId() == R.id.btnCall) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + btnCall.getText().toString()));
            startActivity(callIntent);
        } else if (view.getId() == R.id.btnCar) {
            Intent carActivityIntent = new Intent(mContext, CarActivity.class);
            carActivityIntent.putExtra("name", "Azin");
            startActivity(carActivityIntent);
        }else if (view.getId() == R.id.btnClothing)
        {
            Intent clothingActivityIntent = new Intent(mContext, ClothingActivity.class);
            startActivity(clothingActivityIntent);
        }
    }
}
