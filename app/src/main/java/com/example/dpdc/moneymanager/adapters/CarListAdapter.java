package com.example.dpdc.moneymanager.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dpdc.moneymanager.R;

/**
 * Created by DPDC on 10/19/2017.
 */

public class CarListAdapter extends BaseAdapter {
    Context mContext;
    String cars[];
    int carImages[];

    public CarListAdapter(Context mContext, String[] cars, int carImages[]) {
        this.mContext = mContext;
        this.cars = cars;
        this.carImages = carImages;
    }

    @Override
    public int getCount() {
        return cars.length;
    }

    @Override
    public Object getItem(int i) {
        return cars[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.car_list_item, viewGroup, false);

        TextView carName = (TextView) rowView.findViewById(R.id.carName);
        ImageView carImage = (ImageView) rowView.findViewById(R.id.carImage);
        carName.setText(cars[i]);
        carImage.setImageDrawable(ContextCompat.getDrawable(mContext,carImages[i]));
        return rowView;
    }


}
