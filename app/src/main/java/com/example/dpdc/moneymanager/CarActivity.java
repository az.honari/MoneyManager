package com.example.dpdc.moneymanager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.dpdc.moneymanager.adapters.CarListAdapter;

/**
 * Created by DPDC on 10/19/2017.
 */

public class CarActivity extends BaseActivity implements View.OnClickListener {

    ListView lstCars;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);

        bindViews();

        String cars[] = {
                "Tiba2",
                "Benz",
                "BMW",
                "Bugatti",
                "folex",
                "lamborghini",
                "landcruiser",
                "mazda",
                "neysan",
                "maserati",
                "ferrari",
                "patrol",
                "rollsroyce",
                "santafe"
        };

        int image[] = {
                R.drawable.tiba2,
                R.drawable.benz,
                R.drawable.bmw,
                R.drawable.bugatti,
                R.drawable.folex ,
                R.drawable.lamborghini,
                R.drawable.landcruiser,
                R.drawable.mazda,
                R.drawable.neysan,
                R.drawable.maserati,
                R.drawable.ferrari,
                R.drawable.patrol,
                R.drawable.rollsroyce,
                R.drawable.santafe

        };

        CarListAdapter carListAdapter = new CarListAdapter(mContext, cars,image);

        lstCars.setAdapter(carListAdapter);
        lstCars.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String clickedItem = (String) adapterView.getItemAtPosition(i);
                showToast(clickedItem);
            }
        });
    }

    private void bindViews() {
        lstCars = (ListView) findViewById(R.id.lstCars);
    }

    @Override
    public void onClick(View view) {

    }
}
